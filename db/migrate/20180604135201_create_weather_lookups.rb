class CreateWeatherLookups < ActiveRecord::Migration[5.2]
  def change
    create_table :weather_lookups do |t|
      t.string :city
      t.string :country
      t.string :weather
      t.string :temp
      t.string :pressure
      t.string :humidity
      t.string :wind_speed
      t.string :wind_dir
      t.time :sunrise
      t.time :sunset
      t.float :lat, precision: 10, scale: 6
      t.float :lon, precision: 10, scale: 6

      t.timestamps
    end
  end
end
