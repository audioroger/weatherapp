# Weather App

Is it hot in here? Use this app to get the current temperature for any city on Earth!

## Stack Info
* Ruby 2.5.1
* Rails 5.2.0
* sqlite3 3.8.10.2

## Dependencies
* sprockets-rails
* bootstrap
* jquery-rails
* sass-rails

## Deployment
* `cd *PATH_TO*/weatherapp`
* `bundle install`
* `rake db:migrate`
* `bin/rails server`
* visit `http://localhost:3000/` in your favorite browser

## Testing
* `rspec spec`

## Feature Roadmap
* ~~Add bootstrap carousel header~~
* ~~Display returned city on a map~~
* ~~Display more than just temperature data~~
* ~~Store historic requests to database~~
* ~~Display historic requests~~
* ~~Move returned data out of flash and into styled div~~
* ~~Create weather_lookup detail page~~
* ~~Paginate table of returned lookups~~
* ~~Lookup local time zone for Time displays~~
* Add reCAPTCHA
* Limit lookups from same location
* Get timezone of weather_lookup city to set sunrise and sunset

## General To-Do Items
* Write model tests
* Create test factory for weather_lookups
* Test actual objects instead of just view items