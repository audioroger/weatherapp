class WeatherLookupsController < ApplicationController

  def index
    if(!flash[:alert] && params[:weather_lookup])
      @weather_lookup = WeatherLookup.find(params[:weather_lookup])
    end
    
    @weather_lookups  = WeatherLookup.all.order("created_at DESC").paginate(page: params[:page], per_page: 10)
  end
  
  def show
    @weather_lookup = WeatherLookup.find(params[:id])
  end
  
  def create
    #Get the unit type from the form. Default to "imperial" if none passed
    if(params[:units] == "imperial")
      units = params[:units]
      unit_label_temp = "&deg;F"
      unit_label_sind_speed = "mph"
    elsif(params[:units] == "celsius")
      units = params[:units]
      unit_label_temp = "&deg;C"
      unit_label_sind_speed = "m/s"
    else
      units = "imperial"
      unit_label_temp = "&deg;F"
      unit_label_sind_speed = "mph"
    end
    
    #Set and modify city & state variables from params (could validate and cleanup input here)
    city = params[:city].capitalize
    country = params[:country].upcase
    
    #Setup API URL params
    url = URI("http://api.openweathermap.org/data/2.5/weather?APPID=548f74cb6175f246fce62d7bca56a380&q=#{city},#{country}&units=#{units}")
  
    #Call API
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["Cache-Control"] = 'no-cache'
    
    #Format API response
    response = http.request(request)
    weather_data = JSON.parse(response.body, object_class: JSON::GenericObject)

    #Display error code, or temperature based on API response
    if(weather_data.cod == "404") #API call failed
      flash[:alert] = "Error: #{weather_data.message}<br>"
      flash[:alert] += "City: #{city}<br>Country: #{country}<br>"
    else #API call success
      
      weather_data['weather'].each do |weather|
        @weather_desc = "#{weather['description'].titlecase}"
      end
      
      #Write session variables for map
      @popover = ""
      @popover += "<b>Weather:</b> <span class='current-weather'>#{@weather_desc}</span><br>"
      @popover += "<b>Temperature:</b> <span class='current-temp'>#{weather_data.main.temp}#{unit_label_temp}</span><br>"
      @popover += "<b>Humidity:</b> <span class='current-humidity'>#{weather_data.main.humidity}%</span>"
      
      @weather_lookup = WeatherLookup.create(
                        :city => weather_data.name,
                        :country => weather_data.sys.country,
                        :weather => @weather_desc,
                        :temp => "#{weather_data.main.temp}#{unit_label_temp}",
                        :pressure => "#{weather_data.main.pressure} h",
                        :humidity => "#{weather_data.main.humidity}%",
                        :wind_speed => "#{weather_data.wind.speed} #{unit_label_sind_speed}",
                        :wind_dir => "#{weather_data.wind.deg}&deg;",
                        :sunrise => Time.at(weather_data.sys.sunrise).utc,
                        :sunset => Time.at(weather_data.sys.sunset).utc,
                        :lat => weather_data.coord.lat.to_f,
                        :lon => weather_data.coord.lon.to_f,
                        :popover => @popover
                      )
    end

    redirect_to root_path(:weather_lookup => @weather_lookup)
  end

end