class AddPopoverToWeatherLookups < ActiveRecord::Migration[5.2]
  def change
    add_column :weather_lookups, :popover, :string
  end
end
