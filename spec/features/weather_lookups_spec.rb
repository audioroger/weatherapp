require "rails_helper"

describe "check elements on page", type: :feature do
  
  it "should load the bootstrap carousel" do
    visit root_path
    expect(page).to have_css(".carousel")
  end
  
  it "should load the site header title" do
    visit root_path
    expect(page).to have_content("Weather Lookup App")
  end
  
end

describe "check current temperature", type: :feature do
  
  context "total success" do
    it "should return card div with current temperature for city entered in imperial units" do
      visit root_path
      fill_in "city", with: "longmont"
      fill_in "country", with: "usa"
      click_button "Get the current weather"
      expect(page).not_to have_css(".alert-danger")
      expect(page).to have_css(".card")
      expect(page).to have_content("Longmont")
      expect(page).to have_content("US")
      within('span.current-temp') do
        expect(page).to have_content("F")
      end
    end
    
    it "should return card div with current temperature for city entered in metric units" do
      visit root_path
      fill_in "city", with: "longmont"
      fill_in "country", with: "usa"
      choose("Celsius")
      click_button "Get the current weather"
      expect(page).not_to have_css(".alert-danger")
      expect(page).to have_css(".card")
      expect(page).to have_content("Longmont")
      expect(page).to have_content("US")
      within('span.current-temp') do
        expect(page).to have_content("C")
      end
    end
    
    it "should return all values" do
      visit root_path
      fill_in "city", with: "longmont"
      fill_in "country", with: "usa"
      click_button "Get the current weather"
      expect(page).to have_css(".current-weather")
      expect(page).to have_css(".current-temp")
      expect(page).to have_css(".current-pressure")
      expect(page).to have_css(".current-humidity")
      expect(page).to have_css(".current-wind-speed")
      expect(page).to have_css(".current-wind-direction")
      expect(page).to have_css(".sunrise")
      expect(page).to have_css(".sunset")
    end
    
  end
  
  context "total failure" do
    it "should return flash alert with error message" do
      visit root_path
      fill_in "city", with: "fakecity"
      fill_in "country", with: "fakecountry"
      click_button "Get the current weather"
      expect(page).to have_css(".alert-danger")
      expect(page).not_to have_css(".card")
      expect(page).to have_content("Error: city not found")
      expect(page).to have_content("Fakecity")
      expect(page).to have_content("FAKECOUNTRY")
    end
  end
  
end

describe "load map", js: true do
  
  it "should load the leaflet map on the main page when a temperature is found" do
    visit root_path
    fill_in "city", with: "longmont"
    fill_in "country", with: "usa"
    click_button "Get the current weather"
    expect(page).to have_css(".leaflet-container")
  end
  
  it "should load the leaflet map on the detail page" do
    visit root_path
    fill_in "city", with: "longmont"
    fill_in "country", with: "us"
    click_button "Get the current weather"
    visit "/weather_lookups/1"
    expect(page).to have_css(".leaflet-container")
  end
  
end

describe "load detail page" do
  
  before do
    visit root_path
    fill_in "city", with: "longmont"
    fill_in "country", with: "us"
    click_button "Get the current weather"
    visit "/weather_lookups/1"
  end
  
  it "should load the detail page" do
    expect(page).to have_content("Historic Lookup")
  end
  
  it "should return all values" do
      expect(page).to have_css(".current-weather")
      expect(page).to have_css(".current-temp")
      expect(page).to have_css(".current-pressure")
      expect(page).to have_css(".current-humidity")
      expect(page).to have_css(".current-wind-speed")
      expect(page).to have_css(".current-wind-direction")
      expect(page).to have_css(".sunrise")
      expect(page).to have_css(".sunset")
    end
  
end